# Used a venv for dev
# https://thecodeblogger.com/2020/09/24/wsl-setup-vs-code-for-python-development/
# Activate: source .venv/bin/activate
# Deactivate: deactivate 


import requests
import bs4
from bs4 import BeautifulSoup

def single_page_scrape(url):

    download_links_dict = {}

    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    episode_items = soup.find_all(class_="item podcast-episode")
    
    for episode in episode_items:
        # Extract title
        title_item = episode.find(class_='title')
        
        # Account for two title formattings
        # First works for those with link to episode description on npr.org
        # Second works for those without link 
        try:
            title_element = title_item.find('a')
            title = title_element.contents[0]
        except AttributeError:
            title = title_item.contents[0]
        
        # Extract airdate
        date_item = episode.find(class_='episode-date')
        date_element = date_item.find('time')
        date = date_element.get('datetime')
        
        # Extract download link
        download_item = episode.find(class_="audio-module-listen")
        episode_link = download_item.get('href')
        
        # Add title and link to data structure to return 
        download_links_dict[title] = (date, episode_link)
        
    return download_links_dict

def page_iterator(base_url, range_start, range_end):
    
    mp3_links = {}
    
    for page_num in range(range_start, range_end):
        url = base_url + str(page_num)
        mp3_links.update(single_page_scrape(url))

    return(mp3_links)

def download_all_files(links_to_download):

    for key in links_to_download:
        key_string = key.replace(":","")
        
        download_link = links_to_download[key][1]
        date = links_to_download[key][0]

        doc = requests.get(download_link)
        filename = "Car Talk " + key_string + " (" + date + ")" + ".mp3"
        with open(filename, "wb") as f:
            f.write(doc.content)

base_url = "https://www.npr.org/podcasts/510208/car-talk/partials?start="
range_start = 1
range_end = 736

links_to_download = page_iterator(base_url, range_start, range_end)
download_all_files(links_to_download)